[[_TOC_]]

# Flask api

This is a very basic api project.

[example](https://pythonbasics.org/flask-rest-api/)

## Example Curl stuff

How to use curl (use git bash if windows):
* Put: `curl -H 'Content-Type: application/json' -X PUT -d '{"name":"static","email":"someexample@yahoo.com"}' localhost:5000/`
* GET: `curl localhost:5000/?name=static`
